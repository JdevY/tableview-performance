package Dao;

import DB.ConnectDB;
import Entity.SchoolYearEntity;
import Entity.Student;
import Entity.StudentEntity;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;

import java.util.List;

/**
 * Created on 8/22/15.
 *
 * @author @merzak7
 */
public class StudentsDAO {

    Datastore datastore;

    public StudentsDAO() {
        datastore = ConnectDB.getDatastore();
    }

    public StudentEntity findStudentsById(String id){
        ObjectId oid = new ObjectId(id);
        return datastore.find(StudentEntity.class).field("_id").equal(oid).get();
    }

    public List<StudentEntity> findStudentsBySchoolYear(String year) {
        return datastore.find(StudentEntity.class).field("yearList.schoolYear").equal(year).asList();
    }

    public List<Student> findStudentsBySchoolYearMinInfo(String year) {
        return datastore.find(Student.class).field("yearList.schoolYear").equal(year).asList();
    }

    public boolean existStudent(String schoolYear,ObjectId id){
        Query<StudentEntity> query=datastore.find(StudentEntity.class);
        boolean bool=false;
        query.and(
                query.criteria("yearList.schoolYear").equal(schoolYear),
                query.criteria("_id").equal(id)
        );
        if(query.countAll()>0){bool=true;}
        return bool;
    }

    public Key<StudentEntity> addStudent(StudentEntity student) {
      return  datastore.save(student);
    }

    public boolean deleteStudent(String id) {
        boolean bool = false;
        ObjectId oid = new ObjectId(id);
        WriteResult writeResult = datastore.delete(StudentEntity.class, oid);
        if (writeResult.getN() != 0) {
            bool = true;
        }
        return bool;
    }

    public Key<StudentEntity> updateStudent(StudentEntity s){
        return datastore.save(s);
    }
    // SchoolYear DAO methods
    public List<SchoolYearEntity> getSchoolYears(){
        return datastore.find(SchoolYearEntity.class).asList();
    }

    public void addSchoolYear(SchoolYearEntity schoolYear){
        datastore.save(schoolYear);
    }

    public boolean existSchoolYear(String year){
        boolean exist=false;
        SchoolYearEntity y=datastore.find(SchoolYearEntity.class).field("schoolYear").equal(year).get();
        if(y!=null){
            exist=true;
        }
        return exist;
    }


}
