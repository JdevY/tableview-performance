package DB;

/**
 * Created by merzak7 on 8/17/15.
 */
public interface DBconf {
    String HOST = "localhost";
    int PORT = 27017;

    String DEFAULT_DATABASE = "studentsmsdev";
}
