package DB;


import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created on 8/17/15.
 *
 * @author @merzak7
 */
public class ConnectDB {

    private static Morphia morphia;
    private static Datastore datastore;


    static {
        System.out.println("Try to connect to the database...");

        Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
        mongoLogger.setLevel(Level.SEVERE);

        MongoClient mongoClient = new MongoClient(DBconf.HOST, DBconf.PORT);
        String errorTest;
        try {
            errorTest = mongoClient.getConnectPoint();
        } catch (MongoException me) {
            errorTest=null;
            System.out.println(me.getMessage());
        }
        if (errorTest!=null) {
            morphia = new Morphia();
            datastore = morphia.createDatastore(mongoClient, DBconf.DEFAULT_DATABASE);
            // morphia.mapPackage("org.mongodb.morphia.Entity");
            System.out.println("Connected..");
        }else {

        }

    }

    public static Datastore getDatastore() {
        return datastore;
    }

    public void colseDb() {
        morphia = null;
        datastore = null;
    }
}

