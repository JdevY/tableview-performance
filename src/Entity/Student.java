package Entity;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import java.util.ArrayList;

/**
 * Created by yahia on 6/16/16.
 */

@Entity(value = "students",noClassnameStored=true)
public class Student {

    @Id
    @Property("_id")
    private ObjectId id_student;

    private String fname;
    private String lname;
    private String nameFather;
    private String nameMother;
    private String birthDay;
    private String birthPlace;
    private String tel;
    private String email;
    private String facebook;
    private String photo;
    private String studyLevel;
    private String diplome;
    private String updateDate;
    private String speciality;
    private String nameEstab;
    private String wilayaEstab;

    @Embedded
    private ArrayList<Year> yearList;

    public ObjectId getId_student() {
        return id_student;
    }

    public void setId_student(ObjectId id_student) {
        this.id_student = id_student;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getNameFather() {
        return nameFather;
    }

    public void setNameFather(String nameFather) {
        this.nameFather = nameFather;
    }

    public String getNameMother() {
        return nameMother;
    }

    public void setNameMother(String nameMother) {
        this.nameMother = nameMother;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public ArrayList<Year> getYearList() {
        return yearList;
    }

    public void setYearList(ArrayList<Year> yearList) {
        this.yearList = yearList;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(String studyLevel) {
        this.studyLevel = studyLevel;
    }

    public String getDiplome() {
        return diplome;
    }

    public void setDiplome(String diplome) {
        this.diplome = diplome;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getNameEstab() {
        return nameEstab;
    }

    public void setNameEstab(String nameEstab) {
        this.nameEstab = nameEstab;
    }

    public String getWilayaEstab() {
        return wilayaEstab;
    }

    public void setWilayaEstab(String wilayaEstab) {
        this.wilayaEstab = wilayaEstab;
    }

}
