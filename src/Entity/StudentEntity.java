package Entity;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import java.util.ArrayList;

/**
 * Created by yahia on 26/06/15.
 */

@Entity(value = "students",noClassnameStored=true)
public class StudentEntity {

    @Id
    @Property("_id")
    private ObjectId id_student;

    private String fname;
    private String lname;
    private String nameFather;
    private String nameMother;
    private String birthDay;
    private String birthPlace;
    private String castle;
    private String tribu;
    private String sexe;
    private String bloodType;
    private String dateCreation;
    private String photo;
    private String tel;
    private String email;
    private String facebook;

    @Embedded
    private YearEntity year;
    @Embedded
    private BacEntity bac;
    @Embedded
    private ArrayList<YearEntity> yearList;
    @Embedded
    private ArrayList<BacEntity> bacList;

    public StudentEntity() {
    }

    public StudentEntity(ObjectId id_student, String fname, String lname, String nameFather, String nameMother,
                         String birthDay, String birthPlace, String castle, String tribu,
                         String sexe, String bloodType, String dateCreation,
                         String photo){
       this.id_student=id_student;
       this.fname=fname;
       this.lname=lname;
       this.nameFather=nameFather;
       this.nameMother=nameMother;
       this.birthDay=birthDay;
       this.birthPlace=birthPlace;
       this.castle=castle;
       this.tribu=tribu;
       this.sexe=sexe;
       this.bloodType=bloodType;
       this.dateCreation = dateCreation;
       this.photo=photo;
    }
    public StudentEntity(String fname, String lname, String nameFather, String birthDay, String birthPlace, String castle,
                         String tribu, String bloodType, YearEntity year, BacEntity bac) {
        this.fname=fname;
        this.lname=lname;
        this.nameFather=nameFather;
        this.birthDay=birthDay;
        this.birthPlace=birthPlace;
        this.castle=castle;
        this.tribu=tribu;
        this.bloodType=bloodType;
        this.year=year;
        this.bac=bac;
    }
//-------------------------------------------------------
    public ObjectId getId_student() {return id_student;}
    public void setId_student(String id){this.id_student=new ObjectId(id);}
//-------------------------------------------------------
    public String getFname() {return fname;}
    public void setFname(String fname) {this.fname = fname;}
//-------------------------------------------------------
    public String getLname() {return lname;}
    public void setLname(String lname) {this.lname = lname;}
//-------------------------------------------------------
    public String getNameFather() {return nameFather;}
    public void setNameFather(String nameFather) {this.nameFather = nameFather;}
//-------------------------------------------------------
    public String getNameMother() {return nameMother;}
    public void setNameMother(String nameMother) {this.nameMother = nameMother;}
//-------------------------------------------------------
    public String getBirthDay() {return birthDay;}
    public void setBirthDay(String birthDay) {this.birthDay = birthDay;}
//-------------------------------------------------------
    public String getBirthPlace() {return birthPlace;}
    public void setBirthPlace(String birthPlace) {this.birthPlace = birthPlace;}
//-------------------------------------------------------
    public String getCastle() {return castle;}
    public void setCastle(String castle) {this.castle = castle;}
//-------------------------------------------------------
    public String getTribu() {return tribu;}
    public void setTribu(String tribu) {this.tribu = tribu;}
//-------------------------------------------------------
    public String getSexe() {
        return sexe;
    }
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
//-------------------------------------------------------
    public String getBloodType() {
        return bloodType;
    }
    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }
//-------------------------------------------------------
    public String getDateCreation() {
        return dateCreation;
    }
    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }
//-------------------------------------------------------
    public String getPhoto() {
        return photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
//-------------------------------------------------------
    public String getTel() {
    return tel;
}
    public void setTel(String tel) {
        this.tel = tel;
    }
//-------------------------------------------------------
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
//-------------------------------------------------------
    public String getFacebook() {
        return facebook;
    }
    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }
//-------------------------------------------------------
    public YearEntity getYear() {
        return year;
    }
    public void setYear(YearEntity year) {
        this.year = year;
    }
//-------------------------------------------------------
    public BacEntity getBac() {
        return bac;
    }
    public void setBac(BacEntity bac) {
        this.bac = bac;
    }
//-----------------------------------------------------------------
    public void setYearList(ArrayList<YearEntity> yearList) {
        this.yearList = yearList;
    }
    public ArrayList<YearEntity> getYearList() {
        return yearList;
    }
//------------------------------------------------------------------
    public ArrayList<BacEntity> getBacList() {
        return bacList;
    }
    public void setBacList(ArrayList<BacEntity> bacList) {
        this.bacList = bacList;
    }
//------------------------------------------------------------------
}




