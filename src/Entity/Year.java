package Entity;

import org.mongodb.morphia.annotations.Embedded;

/**
 * Created by yahia on 6/16/16.
 */


@Embedded
public class Year {



    private String schoolYear;
    private String updateDate;

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
