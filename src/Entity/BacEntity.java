package Entity;

import org.mongodb.morphia.annotations.Embedded;

/**
 * Created by yahia on 26/06/15.
 */
@Embedded
public class BacEntity {

    private String yearBac;
    private String brunchBac;
    private String schoolBac;
    private String wilayaBac;
    private String averagBac;

    public BacEntity() {
    }

    public BacEntity(String yearBac, String brunchBac, String schoolBac, String wilayaBac, String averagBac) {
        this.yearBac = yearBac;
        this.brunchBac = brunchBac;
        this.schoolBac = schoolBac;
        this.wilayaBac = wilayaBac;
        this.averagBac=averagBac;
    }

    public String getYearBac() {
        return yearBac;
    }

    public void setYearBac(String yearBac) {
        this.yearBac = yearBac;
    }

    public String getBrunchBac() {
        return brunchBac;
    }

    public void setBrunchBac(String brunchBac) {
        this.brunchBac = brunchBac;
    }

    public String getSchoolBac() {
        return schoolBac;
    }

    public void setSchoolBac(String schoolBac) {
        this.schoolBac = schoolBac;
    }

    public String getWilayaBac() {
        return wilayaBac;
    }

    public void setWilayaBac(String wilayaBac) {
        this.wilayaBac = wilayaBac;
    }

    public String getAveragBac() {
        return averagBac;
    }

    public void setAveragBac(String averagBac) {
        this.averagBac = averagBac;
    }

}




