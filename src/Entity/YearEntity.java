package Entity;

import org.mongodb.morphia.annotations.Embedded;

/**
 * Created by yahia on 26/06/15.
 */
@Embedded
public class YearEntity {

    private String studyState;
    private String studyLevel;
    private String speciality;
    private String diplome;
    private String natureSystem;
    private String natureEstab;
    private String nameEstab;
    private String wilayaEstab;
    private String compus;
    private String yearGraduate;
    private String subjectGraduate;
    private String street;
    private String city;
    private String socialSituation;
    private String parentSocialSituation;
    private String healthSituation;
    private String notes;
    private String schoolYear;
    private boolean updateBool;
    private String updateDate;

    public YearEntity() {
    }

    public String getStudyState() {
        return studyState;
    }

    public void setStudyState(String studyState) {
        this.studyState = studyState;
    }

    public String getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(String studyLevel) {
        this.studyLevel = studyLevel;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDiplome() {
        return diplome;
    }

    public void setDiplome(String diplome) {
        this.diplome = diplome;
    }

    public String getNatureSystem() {
        return natureSystem;
    }

    public void setNatureSystem(String natureSystem) {
        this.natureSystem = natureSystem;
    }

    public String getNatureEstab() {
        return natureEstab;
    }

    public void setNatureEstab(String natureEstab) {
        this.natureEstab = natureEstab;
    }

    public String getNameEstab() {
        return nameEstab;
    }

    public void setNameEstab(String nameEstab) {
        this.nameEstab = nameEstab;
    }

    public String getWilayaEstab() {
        return wilayaEstab;
    }

    public void setWilayaEstab(String wilayaEstab) {
        this.wilayaEstab = wilayaEstab;
    }

    public String getCompus() {
        return compus;
    }

    public void setCompus(String compus) {
        this.compus = compus;
    }

    public String getYearGraduate() {
        return yearGraduate;
    }

    public void setYearGraduate(String yearGraduate) {
        this.yearGraduate = yearGraduate;
    }

    public String getSubjectGraduate() {
        return subjectGraduate;
    }

    public void setSubjectGraduate(String subjectGraduate) {
        this.subjectGraduate = subjectGraduate;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSocialSituation() {
        return socialSituation;
    }

    public void setSocialSituation(String socialSituation) {
        this.socialSituation = socialSituation;
    }

    public String getParentSocialSituation() {
        return parentSocialSituation;
    }

    public void setParentSocialSituation(String parentSocialSituation) {
        this.parentSocialSituation = parentSocialSituation;
    }

    public String getHealthSituation() {
        return healthSituation;
    }

    public void setHealthSituation(String healthSituation) {
        this.healthSituation = healthSituation;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public boolean isUpdateBool() {
        return updateBool;
    }

    public void setUpdateBool(boolean updateBool) {
        this.updateBool = updateBool;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}




