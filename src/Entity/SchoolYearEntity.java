package Entity;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by yahia on 3/22/16.
 */

@Entity
public class SchoolYearEntity {

    @Id
    @Property("_id")
    private ObjectId id_schoolYear;
    private String schoolYear;

    public ObjectId getId_schoolYear() {
        return id_schoolYear;
    }

    public void setId_schoolYear(ObjectId id_schoolYear) {
        this.id_schoolYear = id_schoolYear;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }
}
