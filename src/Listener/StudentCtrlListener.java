package Listener;


import Dao.StudentsDAO;
import Entity.*;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class StudentCtrlListener implements Initializable{
    //------------------------------------------------------------------------
    public ComboBox schoolYearList;
    //------------------------------------------------------------------------
    public TableView<Student> studentTable;
    //------------------------------------------------------------------------
    public TableColumn<Student,String> id_student;
    public TableColumn<Student,String> lname;
    public TableColumn<Student,String> nameFather;
    public TableColumn<Student,String> fname;
    public TableColumn<Student,String> birthDay;
    public TableColumn<Student,String> birthPlace;
    public TableColumn<Student,String> email;
    public TableColumn<Student,String> tel;
    //---------------------------------------------------------------------
    //-----------------------------------------
    @FXML
    public Button addStudentBtn;
    public Button importStudentBtn;
    private static int msg=0;
    public static Stage stageAdd=new Stage(),stageModify=new Stage(),stageMSGS=new Stage();
    public static Scene sceneMSGS,sceneModify;
    @FXML
    public StackPane stackPaneListStudent;
    //---------StudentAddVue-------------------------------------------------------------------
    static ObservableList<Student> data= FXCollections.observableArrayList();
    //---------other vars----------------------------------------------------------------------
    public static FilteredList<Student> filteredList;
    public static String yearComb="";
    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------
    public StudentCtrlListener(){

    }
    @FXML
    public void toYearData(ActionEvent actionEvent) {

        /*Task task=new Task() {
            @Override
            protected Object call() throws Exception {

                return true;
            }
        };
        new Thread(task).start();*/

        Task task=new Task() {
            @Override
            protected Object call() throws Exception {

                yearComb=schoolYearList.getValue().toString();
                if(yearComb.equals("")) {
                    addStudentBtn.setDisable(true);
                    importStudentBtn.setDisable(true);
                }else{
                    addStudentBtn.setDisable(false);
                    importStudentBtn.setDisable(false);
                }
                data.clear();

                getStudentsListByYear();
                filteredList=new FilteredList<Student>(data,p->true);
                studentTable.setItems(filteredList);

                return true;
            }
        };

        new Thread(task).start();


        /*yearComb=schoolYearList.getValue().toString();
        if(yearComb.equals("")) {
            addStudentBtn.setDisable(true);
            importStudentBtn.setDisable(true);
        }else{
            addStudentBtn.setDisable(false);
            importStudentBtn.setDisable(false);
        }
        data.clear();
        //initTableStudent();
        getStudentsListByYear();
        filteredList=new FilteredList<Student>(data,p->true);
        studentTable.setItems(filteredList);*/
    }
    @FXML
    private void toFilterStudent (ActionEvent evt) throws IOException{
        /*try {
            // import data from excel
            File file=new File(getClass().getResource("/home/yahia/Desktop/statistics.xlsx").toURI());
            FileInputStream input=new FileInputStream(file);
            Workbook workbook = WorkbookFactory.create(input);
            Sheet sheet = workbook.getSheetAt(0);
            String data = sheet.getRow(0).getCell(0).getStringCellValue();
            System.out.println(data);
            // execute windows command
            *//*InputStream is=Runtime.getRuntime().exec("cmd /c wmic diskdrive get serialnumber").getInputStream();
            BufferedReader br=new BufferedReader(new InputStreamReader(is));
            String line="",str="";
            while ((line = br.readLine()) != null){
                str=str+line+"\n";
            }
            JOptionPane.showMessageDialog(null,str,"info",JOptionPane.CANCEL_OPTION);*//*

        }catch(Exception e){
            e.printStackTrace();
        }*/
        for(int i=0;i<2;i++){
            StudentEntity student = new StudentEntity();
            Student stud=new Student();

            student.setPhoto("");
            student.setFname("مرزاق");
            student.setLname("مرزاق");
            student.setNameFather("مرزاق");
            student.setNameMother("");
            student.setBirthDay("01/01/2016");
            student.setBirthPlace("");
            student.setCastle("");//combo
            student.setTribu("");
            student.setSexe("");//combo
            student.setBloodType("");//combo
            student.setDateCreation("");
            student.setTel("0556360146");
            student.setEmail("yahia@fg.df");
            student.setFacebook("");


            YearEntity year = new YearEntity();

            year.setStudyState("");//combo
            year.setStudyLevel("");
            year.setSpeciality("");
            year.setDiplome("");//combo
            year.setNatureSystem("");//combo
            year.setNatureEstab("");//combo
            year.setNameEstab("");
            year.setWilayaEstab("");
            year.setCompus("");
            year.setYearGraduate("");
            year.setSubjectGraduate("");
            year.setStreet("");
            year.setCity("");
            year.setSocialSituation("");//combo
            year.setParentSocialSituation("");//combo
            year.setNotes("");
            year.setSchoolYear("2015-2016");
            year.setUpdateBool(true);
            year.setUpdateDate("");

            BacEntity bac = new BacEntity();

            bac.setYearBac("");
            bac.setBrunchBac("");//combo
            bac.setSchoolBac("");
            bac.setWilayaBac("");
            bac.setAveragBac("");


            //-----------------------
            ArrayList years=new ArrayList<YearEntity>();
            years.add(year);
            student.setYearList(years);
            ArrayList bacs=new ArrayList<BacEntity>();
            bacs.add(bac);
            student.setBacList(bacs);


            StudentsDAO studentsDAO = new StudentsDAO();
            studentsDAO.addStudent(student);
        }
    }
    //-------------------------------------------------------------------------------

    //------------------------------------------------------------
    @FXML
    public void initialize(URL location, ResourceBundle resources){

      //-----------------------initDataTable----------------------------------------


        /*id_student.setCellValueFactory(new PropertyValueFactory<Student,String>("id_student") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getId_student().toString());
            }

        });
        fname.setCellValueFactory(new PropertyValueFactory<Student, String>("fname") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getFname());
            }

        });
        lname.setCellValueFactory(new PropertyValueFactory<Student, String>("lname") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getLname());
            }

        });
        nameFather.setCellValueFactory(new PropertyValueFactory<Student, String>("nameFather") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getNameFather());
            }

        });
        birthDay.setCellValueFactory(new PropertyValueFactory<Student, String>("birthDay") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getBirthDay());
            }

        });
        birthPlace.setCellValueFactory(new PropertyValueFactory<Student, String>("birthPlace") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getBirthPlace());
            }

        });
        email.setCellValueFactory(new PropertyValueFactory<Student, String>("email") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getEmail());
            }

        });
        tel.setCellValueFactory(new PropertyValueFactory<Student, String>("tel") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new ReadOnlyObjectWrapper<String>(param.getValue().getTel());
            }

        });*/


        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        id_student.setCellValueFactory(new PropertyValueFactory<Student,String>("id_student") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getId_student().toString());
            }

        });
        fname.setCellValueFactory(new PropertyValueFactory<Student, String>("fname") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getFname());
            }

        });
        lname.setCellValueFactory(new PropertyValueFactory<Student, String>("lname") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getLname());
            }

        });
        nameFather.setCellValueFactory(new PropertyValueFactory<Student, String>("nameFather") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getNameFather());
            }

        });
        birthDay.setCellValueFactory(new PropertyValueFactory<Student, String>("birthDay") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getBirthDay());
            }

        });
        birthPlace.setCellValueFactory(new PropertyValueFactory<Student, String>("birthPlace") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getBirthPlace());
            }

        });
        email.setCellValueFactory(new PropertyValueFactory<Student, String>("email") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getEmail());
            }

        });
        tel.setCellValueFactory(new PropertyValueFactory<Student, String>("tel") {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
                return new SimpleStringProperty(param.getValue().getTel());
            }

        });




        yearComb="2015-2016";
        getStudentsListByYear();


        //-----------------------------------------------------------------------------------------

      /*id_student.setCellValueFactory(new PropertyValueFactory<Student, String>("id_student"));
      fname.setCellValueFactory(new PropertyValueFactory<Student, String>("fname"));
      lname.setCellValueFactory(new PropertyValueFactory<Student, String>("lname"));
      nameFather.setCellValueFactory(new PropertyValueFactory<Student, String>("nameFather"));
      birthDay.setCellValueFactory(new PropertyValueFactory<Student, String>("birthDay"));
      birthPlace.setCellValueFactory(new PropertyValueFactory<Student, String>("birthPlace"));
      email.setCellValueFactory(new PropertyValueFactory<Student, String>("email"));
      tel.setCellValueFactory(new PropertyValueFactory<Student, String>("tel"));*/

        studentTable.setItems(data);

        Image img=new Image("/Vue/images/student.png");

    }


    public  void getStudentsListByYear(){

        StudentsDAO studentsDAO=new StudentsDAO();
        List<Student> studentList=studentsDAO.findStudentsBySchoolYearMinInfo(yearComb);//schoolYear ??
        Year year;

        for (Student student : studentList) {

            //------------------------------------------------
            ArrayList<Year> years=student.getYearList();
            for(int i=0;i<years.size();i++){
                if(years.get(i).getSchoolYear().equals(yearComb)){//schoolYear ??
                    year=years.get(i);
                    break;
                }
            }
            //------------------------------------------------
            data.add(student);
        }
    }

    public LocalDate transferDate(String date) {
        LocalDate localDate;
        if (!date.equals("")) {
            try {
                return localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            } catch (Exception e) {
                try {
                    return localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("M/dd/yyyy"));
                } catch (Exception e1) {
                    try {
                        return localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("M/d/yyyy"));
                    } catch (Exception e2) {
                        return localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("MM/d/yyyy"));
                    }
                }
            }
        }
        return null;
    }














}
