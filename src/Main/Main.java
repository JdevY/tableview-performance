package Main;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/Vue/StudentListVue.fxml"));

        Scene scene=new Scene(root, 400,500);

        primaryStage.setScene(scene);
        primaryStage.setAlwaysOnTop(true);
        primaryStage.setTitle("Students List");


        primaryStage.show();

        String str="sd";
        System.out.println("regulara expression str " + str.matches("\\p{InArabic}"));

    }


    public static void main(String[] args) {
        launch(args);
    }
}
